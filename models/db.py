# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# Validadores personalizados
# -------------------------------------------------------------------------
from validators import *

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------
if request.global_settings.web2py_version < "2.14.1":
    raise HTTP(500, "Requires web2py 2.13.3 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# app configuration made easy. Look inside private/appconfig.ini
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
import time
myconf = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(myconf.get('db.uri'),
             pool_size=myconf.get('db.pool_size'),
             migrate_enabled=myconf.get('db.migrate'),
             lazy_tables=True,
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = ['*'] if request.is_local else []
# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = myconf.get('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.get('forms.separator') or ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

from gluon.tools import Auth, Service, PluginManager

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=myconf.get('host.names'))
service = Service()
plugins = PluginManager()

# -------------------------------------------------------------------------
# create all tables needed by auth if not custom tables
# -------------------------------------------------------------------------
auth.define_tables(username=False, signature=False)

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else myconf.get('smtp.server')
mail.settings.sender = myconf.get('smtp.sender')
mail.settings.login = myconf.get('smtp.login')
mail.settings.tls = myconf.get('smtp.tls') or False
mail.settings.ssl = myconf.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)


db.define_table("presentaciones_egreso",
    Field("descripcion", "string", default=None, label=T("Description")),
    Field("valor", "integer", default=None))

db.define_table("presentacion_ingreso",
    Field("descripcion", "string", default=None))

db.define_table("pescador",
    Field("Apellido", "string", label=T("Surname"), required=True),
    Field("Nombre", "string", label=T("Name"), required=True),
    Field("Domicilio", "text", length=100, label=T("Address"), required=True),
    Field("Cuit", requires=ES_CUIT(), label=T("CUIT"), required=True),
    Field("Nro_Permiso_Pesca", "integer", required=True),
    Field("Vencimiento_Permiso", "date", label=T("Vencimiento de Permiso")),
    Field("Telefono", "integer", label=T("Teléfono")),
    Field("Titulacion", "string",label=T("Titulación")),
    Field("Tipo_Pescador", "string", label=T("Tipo de Actividad")),
    Field("Venc_Fiscalizacion_Sanitaria", "date",label=T("Vencimiento Fiscalización Sanitaria")),
    Field("Vehiculo", "reference vehiculos",
        requires=IS_IN_DB(db,"vehiculos.id","%(Dominio)s"), label=T("Vehicle")))

db.define_table("vehiculos",
    Field("Apellido", "string", default=None, label=T("Surname")),
    Field("Nombre", "string", default=None, label=T("Name")),
    Field("Marca", "string", default=None),
    Field('Modelo',requires=IS_NOT_EMPTY(), required=True),
    Field("Dominio", requires=ES_PATENTE(), default=None, label=T("License N°")),
    Field("Ano", "integer", length=4, notnull=True, default=None, label=T("Year")),
    Field("Vencimiento", "date", default=None), #TODO ¿Vencimiento de que?
    Field("Expediente", "string", default=None)) #TODO ¿Expediente Municipal?

db.define_table("embarcaciones",
    Field("Apellido", "string", default=None, label=T("Surname")),
    Field("Nombre", "string", default=None, label=T("Name")),
    Field("Nombre_Embarcacion", "string", default=None, label=T("Ship Name")),
    Field("Matricula", "string", default=None, label=T("License N°")),
    Field("Eslora", "integer", default=None, label=T("Length")),
    Field("Manga", "integer", default=None, label=T("Width")),
    Field("Puntal", "integer", default=None, label=T("Pitprop")),
    Field("Potencia", "integer", default=None, label=T("Power")),
    Field("Arte_de_Pesca", "string", default=None, label=T("Fishing Art")))

db.define_table("produccion_ingreso",
    Field("fecha", "date", 'datetime',
          default = request.now,
          writable= False,
          requires = IS_DATE(format=('%d/%m/%Y')),
          required = True),
    Field("hora", "time",
          requires = IS_TIME(),
          required = True,
          writable = False,
          default = time.strftime('%H:%M:%S')),
    Field("pescador", "string", "reference pescador",
          required = True),
    Field("embarcacion", "integer", default=None,
          required = True),
    Field("vehiculo_entrega", "string", default=None,
          required = True ),
    Field("temperatura", "integer",),
    Field("vehiculo_limpieza_higiene", "string"),
    Field("vehiculo_contaminantes", "string"),
    Field ("estado", "boolean", default=False, readable=False, writable=False))

db.define_table("orden_ingreso",
    Field("lote", "reference produccion_ingreso", requires=IS_IN_DB(db, "produccion_ingreso")),
    Field("materia_prima", "string", default=None),
    Field("cantidad_individuos", "integer", default=None),
    Field("peso", "integer", default=None),
    Field("puntuacion_demeritos", "integer", default=None),
    Field("descarte", "integer", default=0),
    Field("ingreso_final_total", "integer", default=None),
    Field("toxina_paralizante", "string", default=None, requires=IS_IN_SET(["PRESENTE","NO PRESENTE", "NO APLICA"])),
    Field("toxina_diarreica", "string", default=None, requires=IS_IN_SET(["PRESENTE","NO PRESENTE", "NO APLICA"])),
    Field("toxina_amnesica", "string", default=None, requires=IS_IN_SET(["PRESENTE","NO PRESENTE", "NO APLICA"])),
    Field("observaciones", "string", default=None),
    Field("zona_de_captura", "string", default=None,required = True),
    Field("cerrado", "boolean", default=False, readable=False, writable=False))


db.define_table("produccion_egreso",
    Field("ingreso_n", "integer", default=None),
    Field("egreso_n", "integer", default=None),
    Field("vehiculo_egreso", "string", default=None),
    Field("n_orden","integer",default=None))


db.define_table("orden_egreso",
    Field("n_orden", "integer", default=None),
    Field("descripcion", "string",default=None),
    Field("peso", "integer", default=None))

""" Tablas auxiliares """

db.define_table("titulaciones",
    Field("titulacion", "string", default=None))




""" Relaciones entre tablas"""
# Relaciones y validadores tabla ingreso
#db.produccion_ingreso.pescador.requires = IS_IN_DB(db,'pescador.Apellido')
#db.produccion_ingreso.embarcacion.requires = IS_IN_DB(db,'embarcaciones.Nombre_Embarcacion')
#db.produccion_ingreso.vehiculo_entrega.requires = IS_IN_DB(db, "vehiclos.dominio")
#db.produccion_ingreso.vehiculo_limpieza_higiene.requires = IS_IN_SET(["CORRECTO","DEFICIENTE"])
#db.produccion_ingreso.vehiculo_contaminantes.requires = IS_IN_SET(["CORRECTO","DEFICIENTE"])
#db.produccion_ingreso.toxina_paralizante.requires = IS_IN_SET(["PRESENTE","NO PRESENTE"])
#db.produccion_ingreso.toxina_diarreica.requires = IS_IN_SET(["PRESENTE","NO PRESENTE"])
#db.produccion_ingreso.toxina_amnesica.requires = IS_IN_SET(["PRESENTE","NO PRESENTE"])

#Relaciones y validadores tabla orden
# Solo se pueden cargar órdenes en lotes abiertos
# gdb.orden_ingreso.lote.requires= IS_IN_DB(db, "produccion_ingreso.id")


# Relaciones y validadores tabla Pescador
#db.pescador.Titulacion.requires=IS_IN_DB(db,'titulaciones.titulacion')




#db.pescador.apellido.requires=IS_IN_DB( db, 'produccion_ingreso.id', ' %(fecha)s %(hora)s %(ingreso_n)s %(lote_n)s %(pescador_apellido)s %(embarcacion)s %(vehiculo_entrega)s %(zona_de_captura)s %(puntuacion_demeritos)s %(n_orden)s %(descarte)s %(ingreso_final_total)s %(examen_toxinas)s %(temperatura)s %(vehiculo_limpieza_higiene)s %(vehiculo_contaminantes)s')
#db.vehiculos_habilitados.patente.requires=IS_IN_DB( db, 'produccion_egreso.id', ' %(ingreso_n)s %(egreso_n)s %(vehiculo_egreso)s %(n_orden)s')
#db.embarcaciones.matricula.requires=IS_IN_DB( db, 'produccion_ingreso.id', ' %(fecha)s %(hora)s %(ingreso_n)s %(lote_n)s %(pescador_apellido)s %(embarcacion)s %(vehiculo_entrega)s %(zona_de_captura)s %(puntuacion_demeritos)s %(n_orden)s %(descarte)s %(ingreso_final_total)s %(examen_toxinas)s %(temperatura)s %(vehiculo_limpieza_higiene)s %(vehiculo_contaminantes)s')
#db.produccion_ingreso.ingreso_n.requires=IS_IN_DB( db, 'produccion_egreso.id', ' %(ingreso_n)s %(egreso_n)s %(vehiculo_egreso)s %(n_orden)s')
#db.produccion_ingreso.vehiculo_entrega.requires=IS_IN_DB( db, 'vehiculos_habilitados.id', ' %(apellido)s %(nombre)s %(patente)s %(modelo)s %(vencimiento)s %(marca)s %(expediente)s')
#db.produccion_ingreso.n_orden.requires=IS_IN_DB( db, 'orden_ingreso.id', ' %(n_orden)s %(descripcion)s %(cantidad_individuos)s %(peso)s')
#db.produccion_egreso.n_orden.requires=IS_IN_DB( db, 'orden_egreso.id', ' %(n_orden)s %(descripcion)s %(peso)s')
#db.orden_ingreso.descripcion.requires=IS_IN_DB( db, 'presentacion_ingreso.id', ' %(descripcion)s')
#db.orden_egreso.descripcion.requires=IS_IN_DB( db, 'presentaciones_egreso.id', ' %(descripcion)s %(valor)s')
