# coding: utf8
#from gluon.validators import Validator
class ES_CUIT(object):
    """
    Chequea si el valor ingresado es un CUIT Válido.

    Args:
        Ninguno

    Examples:
        Chequear si el siguiente valor es un cuit válido:
        Field("Cuit", requires=ES_CUIT())
    """
    def __init__(self, error_message="CUIT inválido. Formato 12-12345678-12"):
        self.error_message = error_message

    def __call__(self, cuit):
        cuitentero=cuit
        error = None
        # validaciones minimas
        if len(cuit) != 13 or cuit[2] != "-" or cuit[11] != "-": return (cuit,self.error_message)

        base = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2]

        cuit = cuit.replace("-", "") # remuevo las barras

        # calculo el digito verificador:
        aux = 0
        for i in xrange(10):
            aux += int(cuit[i]) * base[i]

        aux = 11 - (aux - (int(aux / 11) * 11))

        if aux == 11:
            aux = 0
        if aux == 10:
            aux = 9
        comprobacion=aux==int(cuit[10])
        if comprobacion == False: return(cuitentero,self.error_message)
        if comprobacion == True: return(cuitentero,None)

class ES_PATENTE(object):
    """
    Chequea si el valor ingresado es una patente argentina valida.
    La patente debe cumplir con el formato ABC123 ó AB123CD

    Args:
        Ninguno

    Examples:
        Chequear si el siguiente valor es una patente válida:
        Field("Patente", requires=ES_PATENTE())
    """

    def __init__(self, error_message="Patente Inválida. Ingrese patentes con formato ABC123 ó AB123CD"):
        self.error_message = error_message

    def __call__(self, patente):

        error = None
        patente=patente.lower().replace(' ', '')
        puntos=0
        datos_patente=[0,0]
        letras = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
        numeros =['0','1','2','3','4','5','6','7','8','9']

    #Comprobar patente vieja
        if len(patente)==6:
            for elemento in patente[0:3]:
                if elemento in letras: puntos+=1

            for elemento in patente[3:6]:
                if elemento in numeros: puntos+=1

    #Comprobar patente nueva
        if len(patente)==7:
            for elemento in patente[0:2]:
                if elemento in letras: puntos+=1

            for elemento in patente[2:5]:
                if elemento in numeros: puntos+=1
            for elemento in patente[5:7]:
                if elemento in letras: puntos+=1

    #Comprobacion del tipo de patente
        datos_patente=[len(patente),puntos]
        #Patentes aceptadas
        if datos_patente==[6,6]: return(patente,None) #Patente vieja
        if datos_patente==[7,7]: return(patente,None) #Patente nueva
        #Patente rechazada
        if datos_patente!=[6,6] and datos_patente!=[7,7]: return(patente,self.error_message)
