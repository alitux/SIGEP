# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    ejemplo="Bienvenido a SIGEPP"

    return dict(ejemplo=ejemplo)

def ingreso():
    ingreso=SQLFORM(db.produccion_ingreso)
    if ingreso.accepts(request,session):
        response.flash = '¡Ingreso Cargado!'
    elif ingreso.errors:
        response.flash = 'El formulario tiene errores'
    else:
        response.flash = 'por favor complete el formulario'
    return dict (ingreso=ingreso)
def orden():
    orden=SQLFORM.smartgrid(db.orden_ingreso, searchable=False, editable=True)
    return dict(orden=orden)

def egreso():
    egreso=SQLFORM(db.produccion_egreso)
    return dict (egreso=egreso)

def pescador():
    pescador=SQLFORM(db.pescador)
    return dict (pescador=pescador)

def embarcacion():
    embarcaciones=SQLFORM(db.embarcaciones)
    return dict (embarcaciones=embarcaciones)

def transporte():
    transporte=SQLFORM(db.vehiculos)
    return dict (transporte=transporte)



def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
